<?php

class User
{
    public function profile()
    {
        return new Profile;
    }
}

class Profile
{
    public function employment()
    {
        return 'web developer';
    }
}

$user = new User;

//echo $user->profile()?->employment();

// or

echo $user->profile()?->employment() ?? 'Not Provided';
