<?php

class Conversation {}

$obj = new Conversation();

// php 7
// switch (get_class($obj)) {
//     case 'Conversation':
//         $type = 'started_conversation';
//         break;

//     case 'Reply':
//         $type = 'replied_to_conservation';
//         break;

//     case 'Comment':
//         $type = 'commented_on_lesson';
//         break;
// }

$type = match ($obj::class){
    'Conversation' => 'started_conversation',
    'Reply' => 'replied_to_conservation',
    'Comment' => 'commented_on_lesson'
};

echo $type;