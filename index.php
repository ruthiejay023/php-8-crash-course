<?php declare(strict_types = 1);

class User 
{
    public function cancel(bool|string $immediate = false)
    {
        var_dump($immediate);
    }
}

$rm = new User;

$rm->cancel();
$rm->cancel(true);
$rm->cancel(false);

$rm->cancel('next week');