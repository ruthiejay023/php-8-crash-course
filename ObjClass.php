<?php

class Reply {}

$obj = new Reply();

// php 7
switch ($obj::class) {
    case 'Conversation':
        $type = 'started_conversation';
        break;

    case 'Reply':
        $type = 'replied_to_conservation';
        break;

    case 'Comment':
        $type = 'commented_on_lesson';
        break;
}

echo $type;