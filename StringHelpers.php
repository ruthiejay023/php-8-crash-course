<?php

# str_starts_with

$id = 'inv_advsjnwj';
$result = str_starts_with($id, 'inv');

var_dump($result);

# str_ends_with

$id = 'advsjnwj_inv';
$result = str_ends_with($id, 'inv');

var_dump($result);

# str_contains

$id = 'advsjnwj_inv';
$result = str_contains($id, 'inv');

var_dump($result);